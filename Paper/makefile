# PROJECT STRUCTURE:
#     ProjectRoot
#       |- tex        # LaTeX file.
#       |- pdf        # PDF document.
#       |- auxiliary  # All nonessential files (auxiliary files).


# ------------------------- GENERAL PROJECT SETTINGS ---------------------------
# Document name without extension (PDF is assumed).
DOCUMENT = Paper

# Mandatory flags to pass to LaTeX make command.
FLAGS = -pdf -pdflatex="pdflatex -halt-on-error" -use-make

# Additional flags to pass to LaTeX make command.
FLAGS += 
# ------------------------------------------------------------------------------


# --------------------------------- PHONY RULES --------------------------------
# Special words. Ignore files with those names.
.PHONY: $(DOCUMENT).pdf all clean purge
# ------------------------------------------------------------------------------


# ------------------------------- DEFAULT TARGET -------------------------------
# Build the main document.
all: $(DOCUMENT).pdf

# Build the PDF from LaTeX file with the same name.
$(DOCUMENT).pdf: $(DOCUMENT).tex
	latexmk $(FLAGS) $(DOCUMENT).tex
# ------------------------------------------------------------------------------


# --------------------------------- CLEANING -----------------------------------
# Remove all nonessential files, but keep PDF document.
clean:
	latexmk -c
	rm -f $(DOCUMENT).bbl $(DOCUMENT).run.xml

# Remove all nonessential files, including the PDF document.
purge:
	latexmk -CA
	rm -f $(DOCUMENT).bbl $(DOCUMENT).run.xml
# ------------------------------------------------------------------------------
