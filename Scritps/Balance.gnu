#!/usr/bin/gnuplot

# Make gnuplot otuput directly to pdf.
set terminal pdf monochrome dashed
set output "Balance.pdf" # Set output file name.

# Set axes style.
set border 3 linestyle 1 linewidth 0.75 # Set bottom and left border style.
set tics nomirror out scale 0.75 # Make graph tics smaller.

# Grid lines style.
set style line 12 linetype 3 linewidth 0.75
set grid linestyle 12

# Set legend font.
set key font 'sans, 12' top center width -7.5

# Set axes range.
set xrange [1.001:1.9]
set yrange [320.2:420]

# Set axes labels.
set xlabel 'Važnost' offset 0.0, 0.25
set ylabel 'Rizik' offset 0.5, 0.0

set lmargin 7
set bmargin 3

# Set risk of the first solution.
SOL1(x) = 372.5

plot "Balance.txt" with lines linestyle 1 title "rješenje variabilnog rizika", \
      SOL1(x) with lines linestyle 2 title "rješenje fiksnog rizika"
