#!/usr/bin/gnuplot

# Make gnuplot otuput directly to pdf.
set terminal pdf monochrome dashed
set output "Parabola.pdf" # Set output file name.

# Set axes style.
set border 3 linestyle 1 linewidth 0.75 # Set bottom and left border style.
set tics nomirror out scale 0.75 # Make graph tics smaller.

# Grid lines style.
set style line 12 linetype 3 linewidth 0.75
set grid linestyle 12

# Disable legent.
unset key

# Set range of X and Y axis.
set xrange [-4:4]
set yrange [-1:10]

# Plot the data.
plot x*x linestyle 1
